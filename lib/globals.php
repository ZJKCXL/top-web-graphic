<?php

class Globals
{
 
  public static $META_DESC_LENGTH_IDEAL = 160; 
  public static $META_KEYWORDS_LENGTH_IDEAL = 160; 
  public static $META_TITLE_LENGTH_IDEAL = 60; 
  public static $META_DESC_LENGTH = 200; 
  public static $META_KEYWORDS_LENGTH = 200; 
  public static $META_TITLE_LENGTH = 70;    
  public static $GLOBAL_CONTACT_EMAIL = "info@byznysprospolecnost.cz";
  public static $GLOBAL_CONTACT_HIM = "BPS"; 
 }
 



?>
