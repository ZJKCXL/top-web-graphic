<?php
namespace String;

class Soundex {
    const DISTANCE_THRESHOLD = 0.75;

    public static function getSoundexRatio($in_search_word, $in_dictionary_word) {
        $sound_s = soundex($in_search_word);
        $sound_d = soundex($in_dictionary_word);

        $result = \String\Similarity::getSimiliarity($sound_s, $sound_d);

        return $result;
    }

}
?>
