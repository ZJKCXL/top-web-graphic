<?php

namespace Tools;

class TimeExt  {
    static public function Now($format = "H:i:s") {
        return date($format,time());
    }

    static public function getHours ($date) {
        return date("H", strtotime($date));
    }
    static public function getMinutes ($date) {
        return date("i", strtotime($date));
    }
    static public function getSeconds ($date) {
        return date("s", strtotime($date));
    }

    static public function shorten($in_time) {
        $result = false;

        $stmp = strtotime(date("Y-m-d")." ".$in_time);
        $result = date("G:i", $stmp);

        return $result;
    }

    static public function diffHours($in_left_time, $in_right_time) {
        $stmp_l = strtotime("2016-01-01 ".$in_left_time);
        $stmp_r = strtotime("2016-01-01 ".$in_right_time);

        $result = round(($stmp_l - $stmp_r)/3600,2);

        return $result;
    }

    static public function compare($left_time, $right_time) {
        $result = false;

        $left_stmp = strtotime("2016-01-01 ".$left_time);
        $right_stmp = strtotime("2016-01-01 ".$right_time);

        if ($left_stmp == $right_stmp) {
            $result = 0;
        } else {
            $result = ($left_stmp > $right_stmp ? -1 : 1);
        }

        return $result;
    }
}

?>
