<?php
namespace Tools;

class Image {
    private static $image_data;
    private static $image_resized;
    private static $image_thumb;

    const MAX_HEIGHT = 800;
    const MAX_WIDTH = 800;
    const MAX_HEIGHT_THUMB = 80;
    const MAX_WIDTH_THUMB = 80;
    const DEFAULT_FILE_FORMAT = "jpg";

    public static function loadImage($in_image_file) {
        $result = false;
        try {
            self::$image_data = new \Imagick(FILE_DIR.$in_image_file);
            $result = true;
        } catch (\Exception $e) {
            \Kernel\Logger::logToFile("Imagick exception met, exception text: ".$e->getMessage(), \Kernel\Logger::LOG_ERROR, $in_image_file);
        }

        return $result;
    }

    public static function resizeImage($create_thumb = true) {
        $result = false;

        try {
            self::$image_resized = clone self::$image_data;
            if ($create_thumb) {
                self::$image_thumb = clone self::$image_data;
            }

            self::$image_resized->resizeImage(self::MAX_HEIGHT, self::MAX_WIDTH, \Imagick::FILTER_LANCZOS, 1, true);
            if ($create_thumb) {
                self::$image_thumb->thumbnailImage(self::MAX_HEIGHT_THUMB, self::MAX_WIDTH_THUMB, true);
            }

            $result = true;
        } catch (\Exception $e) {
            \Kernel\Logger::logToFile("Imagick exception met, exception text: ".$e->getMessage(), \Kernel\Logger::LOG_ERROR);
        }

        return $result;
    }

    public static function saveImage($out_file_name, $save_thumb = true) {
        $result = false;

        try {

            if (self::$image_resized->valid()) {
                self::$image_resized->writeImage(self::DEFAULT_FILE_FORMAT.":".PRODUCT_IMAGES_DIR.$out_file_name);
            }
            if ($save_thumb) {
                if (self::$image_thumb->valid()) {
                    self::$image_thumb->writeImage(self::DEFAULT_FILE_FORMAT.":".PRODUCT_THUMBNAILS_DIR.$out_file_name);
                }
            }
            $result = true;
        } catch (\Exception $e) {
            \Kernel\Logger::logToFile("Imagick exception met, exception text: ".$e->getMessage(), \Kernel\Logger::LOG_ERROR);
        }

        return $result;
    }
}
?>
