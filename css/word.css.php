<?php   header("Content-type: text/css; charset: UTF-8");  ?>
html, body {
font-family:  sans-serif;
 color: #000;
 font-size: 16px;
 
 background-color: #fff;
}
.strategicke{
  color: #000
}
.tematicke {
color: #000
}
.specialni{
color: #000
}
 a{ color: #000; }
 section a:hover{ text-decoration:  none }
 h1, h2,h3,h4,h5 {
   font-family:  'AvenirNextLTPro-Bold', sans-serif;
 }
 h1 {
   font-size: 28px;
 } 
} 
#text td{
vertical-align:top;
}
ul{
 list-style-type: square;
}
 li{
 
}
ul li span, #content li span, #content li label{
 color: #000 ;
}
section  ul li {
  list-style-image: url('../images/sipkali.png')
}
.hidetooltip ul li, #menu ul li {
  list-style-image: none
}
form{
 margin: 0;
 padding: 0;
}
fieldset{
 border: none;
}
#content #text  a.gal img{
 float: left;
 padding: 2px;
 border: 1px solid #e7e7e7;
 margin: 5px 5px 0 0;     
 padding: 5px;
 width: 135px;
}
.imggal{
 margin-top: 1em;
}
#content #text #intext .pparts{
 clear: both;
 margin-top: 0.5em;

}
.mainclr{
 clear: both;
 margin: 0;
 padding: 0;
 height: 0;
 font-size: 0;
 padding: 0;
 height: 0;
 
}
table, tr, th, td {
	border-collapse: collapse;
	border: 1px solid #c0c0c0;
}
table {
	border: 5px solid #c0c0c0;
}
th, td {
	padding: 5px 8px;
	text-align: left;
	vertical-align: top;
}
#content  ul, #content li{
 margin-left: 0;
 padding: 0;
}
#content ul{
 margin-left: 1.5em;
}
#content #sidebox a.new_window_link img, #content a.new_window_link img, img.downimg, #map_canvas img {
 border: none!important;
 padding: 0!important;
 background: transparent!important;
}

#map_canvas{
 border: 1px solid #fff;
 overflow: hidden;
 margin: 1em 0;
}
 
p.odpaly{
 margin: 10px 15px;
}
#content #text .ref fieldset p{
 margin: 10px;
}
 

.pribehy-z-praxe .kat{
  padding: 1em ;
 border: 5px solid #000;
 padding-bottom: 2em;
}

.pribehy-z-praxe #content-right .kat{
 padding-bottom: 1em;
}

ol li{
 margin-bottom: 0.5em;
}
 
.odskok{
 margin-top: 2em;
}
#sidebox .kat{
 padding: 5px;
 font-size: 90%;

}
#sidebox .kat span{
 font-size: 85%;
 margin-bottom: 5px;
 display: block;
}
#sidebox .kat span b{
 font-size: 110%;

}
.xseachinput em{
 display: block;
 floaT: left;
 width: 1.5em;
 text-align: center;
}
.xseachinput em.longer{
 display: block;
 floaT: left;
 width: 5em;
 text-align: left;
 margin: 0 0.5em 1em ;
 padding-right: 1em;
 border-right: 1px solid #efefef;

}
.cuk{
 /*margin-left: 1em;*/
}
.mytab{
 width: 700px;
 overflow: auto;
 background: #fff;
}
.hibox_komunita a.alltext, .hibox_work a.alltext, .hibox_green a.alltext, .hibox_vize a.alltext, .hibox_market a.alltext {
 text-decoration: underline;
 float: right;
 cursor: pointer;
}
.hibox_komunita a.alltext:hover, .hibox_work a.alltext:hover, .hibox_green a.alltext:hover, .hibox_vize a.alltext:hover, .hibox_market a.alltext:hover {
 text-decoration: none;
 float: right;
}
.new{
  background: #ffcbd5 ;
  border: 1px solid red;
  color: red;
  font-size: 95%;
  padding:  1px 3px;
  margin: 0 5px;
}
.euro {
  background: #a1a3ff ;
  border: 1px solid blue;
  color:  blue;
  font-size: 90%;
  padding:  1px 3px;

}
hr{
 
  border: none;
  background: none;
  border-top: 1px dotted #4B646C 
}
ul.odpaltrochu li{
 margin-bottom: 0.5em
}
textarea {
font-family:  sans-serif;
 color: #4b646c;
 font-size: 85%;
 }
 .hibox_csr li {
  padding-bottom: 5px;
  margin-bottom: 5px;
  border-bottom: 2px dotted #D5ED9F  ;
  margin-right:10px;
 }

 p {
   margin: 0;
   padding: 0;
 }

 .error-message {
    background: #FF4B00;
    color: white;
    opacity: 1;
    font-size: 14px;
    padding: 3px;
    border-radius: 2px;
    clear: both;
}
h2.strategicke, h3.strategicke, p.strategicke, h2.tematicke, h3.tematicke, p.tematicke, h2.specialni, h3.specialni, p.specialni   
{ position: relative; padding-bottom: 5px; padding-top: 5px; }

h2.strategicke, h2.tematicke, h2.specialni { 
   background: #86cae8;
   margin-bottom: 0;
   padding-left: 10px;
   padding-left: 10px;
 }
 h2.tematicke {
   background: #f397bb
 }
  h2.specialni {
   background: #daf48e
 }
 h3.strategicke, p.strategicke, h3.tematicke, p.tematicke, h3.specialni, p.specialni {
   background: #d1e9f5;
   padding: 10px;
   margin-top: 5px;
   margin-bottom: 0;
 }
 h3.tematicke, p.tematicke {
    background: #fcd2e1;
 }
 h3.specialni, p.specialni {
    background: #f1fbd3;
 }

 p.strategicke, p.tematicke,  p.specialni { padding-top: 0; margin-top: 0; }
 

 <?php
 require_once("../lib/globals2021.php");
 include "..". Globals::$GLOBAL_SQL_FILE;

 $testQ = "SELECT * FROM `tbl_app_settings_Names` WHERE `topPublic` = 1";
 $testR =    mysql_query($testQ);
 if ($testR && mysql_num_rows($testR)>0)
 {
 while ($testRow = mysql_fetch_array($testR)){ 

    echo ".".$testRow['topClass']." { background-color: rgba(".$testRow['topBg'].",0.2)!important ; padding:10px } 
    ";
echo ".".$testRow['topClass']." h2, ";
    echo ".".$testRow['topClass']." h3
    { background-color: rgb(".$testRow['topBg'].") ; color: #fff; margin-bottom: 0;
      font-weight: 400;
      font-family: 'AvenirNextLTPro', sans-serif;
      font-size: 18px;
      margin: -10px -10px 10px -10px;
      padding: 10px;
      color: #fff;
    } ";

    echo " h2.".$testRow['topClass']   ;
    echo " , h3.".$testRow['topClass']   ; 
    echo " , h3.".$testRow['topClass']." a  
    { background-color: rgb(".$testRow['topBg'].")!important ; color: rgb(".$testRow['Dark']."); margin-bottom: 0;
    
      font-weight: 400;
      font-family: 'AvenirNextLTPro', sans-serif;
      font-size: 18px;
      padding: 10px;
      color: #fff;

    }";
    echo ".".$testRow['topClass']." h3 ";
    echo " , h3.".$testRow['topClass']."  {    font-size: 15px;   } ";


    echo ".".$testRow['topClass']." h3 { background-color: rgb(".$testRow['topBg'].") ; color: rgb(".$testRow['Dark'].");  margin-bottom: 0 } ";

    echo  "#content-right p.".$testRow['topClass']." a { background-color: rgb(".$testRow['topBg'].") ; color: #fff;   display: inline-block;
      width: auto;
      padding: 5px 10px;
      margin-top: 5px;
      margin-right: 3px;   }";


     echo " form  .".$testRow['topClass']." h4 { margin: -10px -10px 10px -10px ; color: #fff; background-color: rgb(".$testRow['topBg'].") ; font-weight: normal!important; padding: 10px; font-family:  'AvenirNextLTPro-Regular', sans-serif; }  ";


 }
 }
 ?>
 
.kat_clients, .kat  { background:  rgba(64, 64, 64, 0.1)!important;  }
form .kat_clients h4, form  .kat h4 { background:  rgba(64, 64, 64,0.4)!important;   margin: -10px -10px 10px -10px ; color: #fff;   font-weight: normal!important; padding: 10px; font-family:  'AvenirNextLTPro-Regular', sans-serif; }  
table {
    border: 5px solid rgba(64, 64, 64, 0)!important; 
}
table, tr, th, td {
    border-collapse: collapse;
    border: 1px solid rgba(64, 64, 64, 0)!important;
}
h5 { font-size: 15px; margin: 0}